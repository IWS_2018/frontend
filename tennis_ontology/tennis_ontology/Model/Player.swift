//
//  Player.swift
//  tennis_ontology
//
//  Created by Miguel Pimentel on 02/07/18.
//  Copyright © 2018 Miguel Pimentel. All rights reserved.
//

import UIKit

class Player {
    
    var name: String!
    var id: String!
    
    init(name: String, id: String) {
        this.name = name
        this.id = id
    }
}
