# Configurações de Ambiente

**Repositório da aplicação: https://gitlab.com/IWS_2018/frontend**

Para acessá-lo [clique aqui](https://gitlab.com/IWS_2018/frontend).

## 1. Configurando o Sesame(RDF4j)

### 1.1 Pré - Requisitos

* Ter o Docker instalado e configurado, caso não tenha acesse o [link](https://docs.docker.com/install/).

### 1.2 Passos

#### 1.2.1 Instalando e usando o Sesame(RDF4J)


Execute os comando abaixo para baixar e executar a imagem do Sesame na  porta 8080:

```
docker pull subotic/openrdf-sesame
docker run -d -p 8080:8080 subotic/openrdf-sesame
```

Em um navegador acesse o endereço [Sesame - Criar Repositório](http://localhost:8080/openrdf-workbench/repositories/NONE/create")

Preencha o formulário como na imagem abaixo e depois pressione o botão **Next**.

![](tutorial_images/01.png?raw=true)

Na tela seguinte, clique no botão **Create**, como mostrado na imagem abaixo:

![](tutorial_images/02.png?raw=true)

Na tela seguinte, clique em **add**, como mostrado na imagem abaixo:

![](tutorial_images/03.png?raw=true)

Na tela seguinte, preencha o formulário na ordem especificada na imagem:

1. Coloque no campo **RDF Data URL**: https://gitlab.com/IWS_2018/frontend/raw/master/ontology.ttl
2. Altere o campo **Base URI** para: http://nexte.io/ontology
3. Verifique se o campo **Context** está preenchido como: <http://nexte.io/ontology>
4. Selecione no combo box **Data format** a opção: Turtle
5. Clique em **Upload**

![](tutorial_images/04.png?raw=true)

Para realizar uma query, basta clicar em **query**, como mostrado na imagem abaixo:

![](tutorial_images/05.png?raw=true)

Depois adicione uma query e clique **Execute**:

![](tutorial_images/06.png?raw=true)

Os resultados serão mostrados como abaixo:

![](tutorial_images/07.png?raw=true)

Exemplos de queries:

```sparql
# Get Ranking
PREFIX :  <http://nexte.io/ontology/>
SELECT DISTINCT ?posicao ?nomeDoTenista
WHERE {
  ?x :compostoPor ?position.
  ?position :temNome ?posicao;
            :ocupadaPor ?tenista.
  ?tenista :temNome ?nomeDoTenista.
}
```

```sparql
# Get rules from ranking
PREFIX :  <http://nexte.io/ontology/>
SELECT DISTINCT ?Nome ?Descricao
WHERE {
  ?ranking :possuiRegra ?regra.
  ?regra :temDescricao ?Descricao;
    	   :temNome ?Nome
}
```

#### 1.2.2 Usando o Postman para realizar queries

Caso queira usar o protocolo HTTP para manipular o repositório de triplas, basta seguir as queries de exemplo disponibilizadas em formato compatível com a aplicação [Postman](https://www.getpostman.com).

Siga o processo da imagem abaixo e adicione a URL de importação: https://www.getpostman.com/collections/3dcadc5288bf7efb8ad7.

![](tutorial_images/08.png?raw=true)

## 2. Configurando a Aplicação


### 2.1 Pré - Requisitos

* Sistema operacional MacOS

 * XCode(IDE para desenvolvimento iOS)

* CocoaPods instalado, caso não tenha acesse o [link](https://cocoapods.org)


#### 2.1.1 Passo 1 - Instalando dependências externas

 As dependência foram adicionadas a partir do CocoaPods, asssim para adicionar as dependências execute os seguintes passos:

Primeiramente, crie um podfile para isso, abra o diretório central onde existe o arquivo tennis_ontology.xcodeproj e digite:

```
pod init
```

A partir deste comando teremos o arquivo Podfile, abra ele em um editor de sua preferência e insira o códgio abaixo para inserir as dependências.

```
# Uncomment the next line to define a global platform for your project
# platform :ios, '9.0'

target 'tennis_ontology' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!

  pod 'Alamofire', '~> 4.7'
  pod 'SwiftyJSON', '~> 4.0'
  # Pods for tennis_ontology

end
```

Para efetivar as alterações execute o comando:
```
pod install
```

#### 2.1.2 Passo 2 - Executando

Ao realizar o Passo 1 será criado o executável   tennis_ontology.xcworkspace. Assim, abra ele com auxílio do XCode. Em seguida, esolha um dos *devices* para executar parte superior esquerda em simulador e clique no Botão Play(Verde). A Aplicação deverá ser executada normalmente,
